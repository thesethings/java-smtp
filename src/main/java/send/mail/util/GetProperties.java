package send.mail.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GetProperties {

    private static final String FILENAME = "connection.properties";
    private static String smtpServer;
    private static String username;
    private static String password;
    private static String emailFrom;
    private static String emailTo;
    private static String emailSubject;
    private static String emailBody;
    private static String emailHtmlBody;
    private static String attachment;
    private static String transportProtocol;
    private static String smtpPort;

    public GetProperties() {
    }

    public static void run() {

        Properties prop = new Properties();
        InputStream input = null;

        try {

            input = GetProperties.class.getClassLoader().getResourceAsStream(FILENAME);
            if (input == null) {
                System.out.println("Sorry, unable to find " + FILENAME);
                return;
            }

            //load a properties file from class path, inside  method
            prop.load(input);

            setSmtpServer(prop.getProperty("smtp-server"));
            setUsername(prop.getProperty("username"));
            setPassword(prop.getProperty("password"));
            setEmailFrom(prop.getProperty("email-from"));
            setEmailTo(prop.getProperty("email-to"));
            setEmailSubject(prop.getProperty("email-subject"));
            setEmailBody(prop.getProperty("email-body"));
            setAttachment(prop.getProperty("attachment"));
            setTransportProtocol(prop.getProperty("transport-protocol"));
            setSmtpPort(prop.getProperty("smtp-port"));
            setEmailHtmlBody(prop.getProperty("email-html-body"));

            /*
            //test the property value and print it out
            System.out.printf("Hostname: %s%n", getSmtpServer());
            System.out.printf("SMTP Port: ", getSmtpPort());
            System.out.println("User: "+ getUsername());
            System.out.println("Password: "+ getPassword());
            System.out.println("Email from: "+ getEmailFrom());
            System.out.println("Email to: "+ getEmailTo());
            System.out.println("Email Body: "+ getEmailBody());
            */

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    public static String getSmtpServer() {
        return smtpServer;
    }

    public static void setSmtpServer(String smtpServer) {
        GetProperties.smtpServer = smtpServer;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        GetProperties.username = username;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        GetProperties.password = password;
    }

    public static String getEmailFrom() {
        return emailFrom;
    }

    public static void setEmailFrom(String emailFrom) {
        GetProperties.emailFrom = emailFrom;
    }

    public static String getEmailTo() {
        return emailTo;
    }

    public static void setEmailTo(String emailTo) {
        GetProperties.emailTo = emailTo;
    }

    public static String getEmailSubject() {
        return emailSubject;
    }

    public static void setEmailSubject(String emailSubject) {
        GetProperties.emailSubject = emailSubject;
    }

    public static String getEmailBody() {
        return emailBody;
    }

    public static void setEmailBody(String emailBody) {
        GetProperties.emailBody = emailBody;
    }

    public static String getEmailHtmlBody() {
        return emailHtmlBody;
    }

    public static void setEmailHtmlBody(String emailHtmlBody) {
        GetProperties.emailHtmlBody = emailHtmlBody;
    }


    public static String getAttachment() {
        return attachment;
    }

    public static void setAttachment(String attachment) {
        GetProperties.attachment = attachment;
    }

    public static String getTransportProtocol() {
        return transportProtocol;
    }

    public static void setTransportProtocol(String transportProtocol) {
        GetProperties.transportProtocol = transportProtocol;
    }

    public static String getSmtpPort() {
        return smtpPort;
    }

    public static void setSmtpPort(String smtpPort) {
        GetProperties.smtpPort = smtpPort;
    }

}
