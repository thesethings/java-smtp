package send.mail;

import com.sun.mail.smtp.SMTPTransport;
import send.mail.util.GetProperties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

import static send.mail.util.GetProperties.*;

public class SendAnSimpleEmail {

    public static void main(String[] args) {
        GetProperties.run();

        Properties properties = System.getProperties();
        properties.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(properties, null);
        Message mimeMessage = new MimeMessage(session);

        try {
            mimeMessage.setFrom(new InternetAddress(getEmailFrom()));
            mimeMessage.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(getEmailTo(), false));
            mimeMessage.setSubject(getEmailSubject());
            mimeMessage.setText(getEmailBody());
            mimeMessage.setSentDate(new Date());

            SMTPTransport smtp = (SMTPTransport) session.getTransport(getTransportProtocol());

            smtp.connect(getSmtpServer(), getUsername(), getPassword());
            smtp.sendMessage(mimeMessage, mimeMessage.getAllRecipients());

            System.out.println("Server Response: " + smtp.getLastServerResponse());
            smtp.close();

        } catch (MessagingException e) {
            e.printStackTrace();
        }


    }
}

