package send.mail;

import com.sun.mail.smtp.SMTPTransport;
import send.mail.util.GetProperties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import static send.mail.util.GetProperties.*;

public class SendAnEmailInHTMLFormat {

    public static void main(String[] args) {
        GetProperties.run();

        Properties properties = System.getProperties();
        properties.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(properties, null);
        Message mimeMessage = new MimeMessage(session);

        try {

            mimeMessage.setFrom(new InternetAddress(getEmailFrom()));
            mimeMessage.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(getEmailTo(), false));
            mimeMessage.setSubject(getEmailSubject());
            mimeMessage.setDataHandler(new DataHandler(new HTMLDataSource(getEmailHtmlBody())));


            SMTPTransport smtp = (SMTPTransport) session.getTransport(getTransportProtocol());

            // connect
            smtp.connect(getSmtpServer(), getUsername(), getPassword());

            // send
            smtp.sendMessage(mimeMessage, mimeMessage.getAllRecipients());

            System.out.println("Server Response: " + smtp.getLastServerResponse());

            smtp.close();

        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }

    static class HTMLDataSource implements DataSource {

        private String html;

        public HTMLDataSource(String htmlString) {
            html = htmlString;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            if (html == null) throw new IOException("html message is null!");
            return new ByteArrayInputStream(html.getBytes());
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            throw new IOException("This DataHandler cannot write HTML");
        }

        @Override
        public String getContentType() {
            return "text/html";
        }

        @Override
        public String getName() {
            return "HTMLDataSource";
        }
    }
}
