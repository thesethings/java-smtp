package send.mail;

import com.sun.mail.smtp.SMTPTransport;
import send.mail.util.GetProperties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

import static send.mail.util.GetProperties.*;

public class SendAnEmailWithAttachment {

    public static void main(String[] args) {
        GetProperties.run();

        Properties properties = System.getProperties();
        properties.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(properties, null);
        Message mimeMessage = new MimeMessage(session);

        try {

            mimeMessage.setFrom(new InternetAddress(getEmailFrom()));
            mimeMessage.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(getEmailTo(), false));
            mimeMessage.setSubject(getEmailSubject());

            MimeBodyPart mimeBodyPart1 = new MimeBodyPart();
            mimeBodyPart1.setText(getEmailBody());

            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            FileDataSource fileDataSource = new FileDataSource(getAttachment());
            mimeBodyPart.setDataHandler(new DataHandler(fileDataSource));
            mimeBodyPart.setFileName(fileDataSource.getName());

            Multipart mimeMultipart = new MimeMultipart();
            mimeMultipart.addBodyPart(mimeBodyPart1);
            mimeMultipart.addBodyPart(mimeBodyPart);

            mimeMessage.setContent(mimeMultipart);

            SMTPTransport smtp = (SMTPTransport) session.getTransport(getTransportProtocol());
            smtp.connect(getSmtpServer(), getUsername(), getPassword());
            smtp.sendMessage(mimeMessage, mimeMessage.getAllRecipients());

            System.out.println("Server Response: " + smtp.getLastServerResponse());

            smtp.close();

        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }

}

